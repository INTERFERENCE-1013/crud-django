from django.shortcuts import render, redirect
from .models import users
from .forms import UserForm

from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

# Create your views here.

class UserList(ListView):
    model = users
    template_name = 'users.html'

class UserCreate(CreateView):
    model = users
    form_class = UserForm
    template_name = 'users_form.html'
    success_url = reverse_lazy('list_user')

class UserUpdate(UpdateView):
    model = users
    form_class = UserForm
    template_name = 'users_form.html'
    success_url = reverse_lazy('list_user')

class UserDelete(DeleteView):
    model = users
    template_name = 'user_delete_confirm.html'
    success_url = reverse_lazy('list_user')

def WebUpdates(request):
    return render(request, "info_updates.html")

# def list_user(request):

#     usuarios = users.objects.all()
#     return render(request, "users.html", {'users': usuarios})

# def create_user(request):

#     form = UserForm(request.POST or None)

#     if form.is_valid():
#         form.save()
#         return redirect('list_user')
#     return render(request, "users_form.html", {'form': form})

# def update_user(request, id):

#     usuario = users.objects.get(id=id)
#     form = UserForm(request.POST or None, instance = usuario)

#     if form.is_valid():
#         form.save()
#         return redirect('list_user')
#     return render(request, "users_form.html", {'form': form, 'users': usuario})

# def delete_user(request, id):

#     usuario = users.objects.get(id=id)

#     if request.method == 'POST':
#         usuario.delete()
#         return redirect('list_user')

#     return render(request, "user_delete_confirm.html", {'user': usuario})